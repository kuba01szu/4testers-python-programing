from src.guarantee import check_guarantee

def test_check_guarantee_for_lower_age_and_milage():
    assert check_guarantee(2020, 30000) == True

def test_check_guarantee_for_lower_age_and_higher_milage():
    assert check_guarantee(2020, 70000) == False

def test_check_guarantee_for_higher_age_and_lower_milage():
    assert check_guarantee(2016, 30000) == False

def test_check_guarantee_for_higher_age_and_higher_milage():
    assert check_guarantee(2016, 120000) == False