from src.car import Car

def test_increase_car_distance():
    test_car = Car('Dodge', 'red', 2012)
    test_car.drive(100)
    test_car.drive(100)

    assert test_car.milage == 200

def test_get_age():
    test_car = Car('Dodge', 'red', 2012)
    assert test_car.get_age() == 12

def test_repaint():
    test_car = Car('Dodge', 'red', 2012)
    test_car.repaint('blue')
    assert test_car.color == "blue"
