def print_a_car_brand_name():
    print("honda")


def print_given_number_multiply_by_3(imput_number):
    print(imput_number * 3)


def calculate_area_of_a_circle(radius):
    return 3.14 * radius ** 2


def calculate_area_of_a_triangle(bottom, height):
    return 0.5 * bottom * height


print_given_number_multiply_by_3(40)
print_a_car_brand_name()
area_of_a_circle_with_radius_10 = calculate_area_of_a_circle(10)
print(area_of_a_circle_with_radius_10)
area_of_a_little_triangle = calculate_area_of_a_triangle(5, 5)
print(area_of_a_little_triangle)

result_of_print_function = print_a_car_brand_name()
print(result_of_print_function)


def upper_word(word):
    return word.upper()


big_dog = upper_word('dog')
print(big_dog)


def square_given_number(input_number):
    return input_number * input_number


def volume_of_cuboid(input_number1, input_number2, input_number3):
    return input_number1 * input_number2 * input_number3


def convert_celcius_in_farenhait(temp_in_celcius):
    return 9 / 5 * temp_in_celcius + 32


if __name__ == '__main__':
    zero_squared = square_given_number(0)
    sixteen_squared = square_given_number(16)
    float_squared = square_given_number(2.65)
    print('square of 0 is', zero_squared)
    print('square of 16 is', sixteen_squared)
    print('square of 2.65 is', float_squared)

    volume_result = volume_of_cuboid(3, 5, 7)
    print(volume_result)

    farenhait_temp = convert_celcius_in_farenhait(20)
    print('20c degrees is equal to', farenhait_temp, 'farenhait')
