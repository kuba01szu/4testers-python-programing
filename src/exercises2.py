def take_list_3_numbers(number1, number2, number3):
    return [number1 * 3, number2 * 3, number3 * 3]

def take_list_of_4_numbers(no1, no2, no3, no4):
    return [no1 ** 2, no2 ** 2, no3 * no4]

def take_list_of_5_numbers(n1, n2, n3, n4, n5):
    list3 = [n1, n2, n3, n4, n5]
    return len(list3) / 2


if __name__ == '__main__':

    list = take_list_3_numbers(4, 5, 8)
    print(list)

    list2 = take_list_of_4_numbers(6, 4, 8, 5)
    print(list2)

    list3 = take_list_of_5_numbers(1, 2, 3, 4, 5)
    print(list3)
