
from faker import Faker
import uuid

fake = Faker()
def generate_random_password_and_email(email, password):
    email = fake.ascii_safe_email()
    password = uuid.uuid4()
    return {
        'email': email,
        'password': password
        }

if __name__ == '__main__':
    pass_and_email = generate_random_password_and_email('email', 'password')
    print(pass_and_email)

