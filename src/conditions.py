def check_if_standard_condition(temp_in_celcius, pressure_in_hpa):
    if temp_in_celcius == 0 and pressure_in_hpa == 1013:
        return True
    else:
        return False

def get_grade_for_test_percentage(test_percentage):
    if test_percentage >= 90:
        return 5
    elif test_percentage >= 75:
        return 4
    elif test_percentage >=50:
        return 3
    else:
        return 2

if __name__ == '__main__':
    print(f"Checking if standard conditions for 0 Celcius, 1013 hpa: {check_if_standard_condition(0, 1013)}")
    print(f"Checking if standard conditions for 0 Celcius, 1013 hpa: {check_if_standard_condition(0, 1014)}")
    print(f"Checking if standard conditions for 0 Celcius, 1013 hpa: {check_if_standard_condition(1, 1013)}")
    print(f"Checking if standard conditions for 0 Celcius, 1013 hpa: {check_if_standard_condition(1, 1014)}")

    print(f"Grade for 99% is {get_grade_for_test_percentage(99)}")
    print(f"Grade for 75% is {get_grade_for_test_percentage(75)}")
    print(f"Grade for 56% is {get_grade_for_test_percentage(56)}")
    print(f"Grade for 30% is {get_grade_for_test_percentage(30)}")