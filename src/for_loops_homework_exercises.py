def print_names_from_list_capitalize(list_of_names):
    for i in list_of_names:
        print(i.capitalize())

def print_list_of_squared_numbers():
    for a in range(0, 101):
        print(a ** 2)

def print_list_of_animals_upper(list_of_animals):
    for c in list_of_animals:
        print(c.upper())

def print_titles_of_favorite_movies_backwards(list_of_movies):
    for b in list_of_movies:
        print(b[::-1])

def print_titles_of_favorite_bands_upper_backwards(bands):
    for d in bands:
        print(d[::-1].upper())



if __name__ == '__main__':
    list = ['ola', 'daro', 'beN', 'ken']
    print_names_from_list_capitalize(list)


    print(print_list_of_squared_numbers())

    animal_list = ['fox', 'squirrel', 'wolf', 'seal', 'horse', 'snake']
    print(print_list_of_animals_upper(animal_list))

    list_of_favorite_movies = ['Lincoln lawyer', 'Equalizer', 'Next', 'On the knives']
    print(print_titles_of_favorite_movies_backwards(list_of_favorite_movies))

    favorite_bands = ['Cranberies', 'metallica', 'Two steps from hell']
    print(print_titles_of_favorite_bands_upper_backwards(favorite_bands))