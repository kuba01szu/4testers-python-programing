"""
Zadanie 1 - Generacja danych
Skrypt zadanie.py zawiera listy danych:
imiona żeńskie
imiona męskie
nazwiska
nazwy krajów
Poniżej umieszczone są dwa przykłady jak za pomocą funkcji random.choice() i
random.randint() można wylosować przypadkowy element z listy lub zakresu liczb.
Korzystając z tych danych wygeneruj listę słowników, która będzie zawierać 10
elementów. Wymagania:
1. Każdy ze słowników ma zawierać klucze:
firstname -> imię wylosowane z listy imion
lastname -> nazwisko wylosowane z listy nazwisk
country -> kraj wylosowany z listy krajów
email -> postaci: imie.nazwisko@example.com (wszystko małymi literami)
age -> wiek wylosowany z zakresu 5-45 lat
adult -> o wartości True jeśli age >= 18 i False w przeciwnym
przypadku
birth_year -> rok urodzenia (obliczony na podstawie wylowoanego wieku)
2. W liście ma znajdować się po 5 imion męskich i 5 żeńskich (łącznie 10
elementów)
3. Mając utworzoną listę słowników, dla każdego jej elementu proszę o
wygenerowanie przedstawienia danej osoby w postaci następującego opisu
(wartości pisane z dużych liter należy zastąpić kluczami ze słownika):
Hi! I'm FIRSTNAME LASTNAME. I come from COUNTRY and I was born in BIRTH_YEAR
"""

"""
import random

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

# Przykład jak można losować imię z listy
random_female_firstname = random.choice(female_fnames)
print(random_female_firstname)

# Przykład jak można losować wiek z liczb całkowitych od 1 do 65
random_age = random.randint(1, 65)
print(random_age)

# Przykładowy wygenerowany pojedynczy słownik
example_dictionary = {
    'firstname': 'Kate',
    'lastname': 'Yu',
    'email': 'kate.yu@example.com',
    'age': 23,
    'country': 'Poland',
    'adult': True
}
"""

import random
from faker import Faker

fake = Faker

female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

names = female_fnames + male_fnames
age = range(5, 46)

'''def is_adult():
    for x in age:
        x >= 18
        return True
    else:
        False'''



def generate_random_dictionary_from_lists():
    return {
        'firstname': random.choice(names),
        'lastname': random.choice(surnames),
        'email': f"{random.choice(names)}.{random.choice(surnames)}@example.com",
        'age': random.choice(age),
        'country': random.choice(countries),
        'adult': random.choice([True, False])
    }

def generate_set_of_dictionaries(number_of_dictionaries):
    return [generate_random_dictionary_from_lists() for i in range(number_of_dictionaries)]

def get_name_of_employee(list_of_employee):
    output_adult = []

    for employee_dictionary in list_of_employee:
        if employee_dictionary('age') >= 18:
            output_adult.append(employee_dictionary['adult'])
    return
            



'''def get_random_5_female_name_from_list(female_fnames):
    return [random.choice(female_fnames)]


def get_random_5_male_name_from_list(male_fnames):
    return [random.choice(male_fnames)] '''

'''def upper_word(country):
    return country.upper()'''

'''random_female = [random.choice(female_fnames)]
random_male = [random.choice(male_fnames)]
random_name = [random.choice(names)] '''


'''def create_list_of_random_male_and_female_names():
    return [random_name]

def print_each_country_upper():
    for country in countries:
        print_each_country_upper '''

if __name__ == '__main__':

     '''for i in range(5):
         print(get_random_5_female_name_from_list(female_fnames))
         print(get_random_5_male_name_from_list(male_fnames))
         is_adult() '''
     for d in range(20):
        print(generate_random_dictionary_from_lists())


     generated_employess = (generate_set_of_dictionaries(10))
     print(generated_employess)
     