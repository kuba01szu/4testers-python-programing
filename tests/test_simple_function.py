from src.simple_function import is_adult


def test_is_adult_for_age_greater_than_18():
    assert is_adult(19) == True

def test_is_adult_for_age_equal_than_18():
    assert is_adult(18) == True
def test_is_adult_for_age_less_than_18():
    assert is_adult(17) == False

'''def test_listcontaining_all_words_with_letter_a():
    imput_list = lista = ['admin', 'roman', 'budy', 'cord']
    expected_list = ['admin', 'roman']
    assert return_words_containing_letter_a(imput_list) == expected_list'''

