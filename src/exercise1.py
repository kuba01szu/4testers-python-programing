import random
import datetime

female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']




def generate_dictionary(is_female):
    if is_female:
        first_name = random.choice(female_fnames)
    else:
        first_name = random.choice(male_fnames)

    last_name = random.choice(surnames)
    email = f"{first_name.lower()}.{last_name.lower()}@example.org"
    age = random.randint(5, 45)
    current_year = datetime.datetime.now().year - age

    birth_year = current_year
    is_adult = age >= 18
    country = random.choice(countries)

    return {
        'firstname': first_name,
        'lastname': last_name,
        'email': email,
        'age': age,
        'country': country,
        'adult': is_adult,
        'birth_year': birth_year
    }


def is_adult(age):
    if age >= 18:
       return True
    else:
        return False




def generate_list_of_dictionaries(number_of_pairs):
    output_list = []
    for i in range(number_of_pairs):
        output_list.append(generate_dictionary(True))
        output_list.append(generate_dictionary(False))
    return output_list


if __name__ == '__main__':
    print(generate_list_of_dictionaries(5))



       


