def generate_email(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"

def generate_sentence_with_pets_and_owners(owner_name, pet_name, kind_of_animal):
    return f"Hello my name is {owner_name.capitalize()} and I'm owner of {pet_name.capitalize()} and he is a {kind_of_animal.upper()}."

def generate_sentence_who_own_which_car(owner_name, car, color, car_age):
    return f"My name is {owner_name.capitalize()} and I have {color} {car.upper()} which is {car_age} years old."

if __name__ == '__main__':
    januszs_email = generate_email('Janusz', 'Nowak')
    barbaras_email = generate_email('Barbara', 'Kowalska')
    print(januszs_email)
    print(barbaras_email)

    metric1 = generate_sentence_with_pets_and_owners('marcin', 'korek', 'fish')
    metric2 = generate_sentence_with_pets_and_owners('grzesiek', 'abi', 'dog')
    metric3 = generate_sentence_with_pets_and_owners('piotrek', 'dirt', 'rat')
    print(metric1)
    print(metric2)
    print(metric3)

    car1 = generate_sentence_who_own_which_car('Bart', 'Dodge', 'red', 58)
    car2 = generate_sentence_who_own_which_car('anton', 'fiat', 'orange', 38)
    car3 = generate_sentence_who_own_which_car('kris', 'maseratti', 'silver', 3)
    car4 = generate_sentence_who_own_which_car('ann', 'bmw', 'black', 8)
    car5 = generate_sentence_who_own_which_car('hannah', 'citroen', 'white', 6)
    car6 = generate_sentence_who_own_which_car('borys', 'mercedes', 'black', 1)
    print(car1)
    print(car2)
    print(car3)
    print(car4)
    print(car5)
    print(car6)
