first_name = "Kuba"
last_name = "Szuliński"
email = "kuba01szu@gmail.com"

print("Mam na imię ", first_name, ". ", "Moje nazwisko to ", last_name, ". ", "Mój email to ", email, ". ", sep="")

my_bio = "Mam na imię " + first_name + ". Moje nazwisko to " + last_name + ". Mój email to " + email + ". "
print(my_bio)

# F-string
my_bio_using_f_string = f"Mam na imię {first_name}.\nMoje nazwisko to {last_name}.\nMój email to {email}."
print(my_bio_using_f_string)

print(f"wynik mnożenia 4 przez pięć to {4 * 5}")

### Algebra###
circle_radius = 10
area_of_the_circle_with_radius_5 = 3.14 * circle_radius ** 2
circumference_of_the_circle_with_radius_5 = 2 * 3.14 * circle_radius

print("Area of circle with radius 10 is", area_of_the_circle_with_radius_5)
print(f"Circumference of circle with radius {circle_radius} is", circumference_of_the_circle_with_radius_5)

long_mathematical_expression = 2 + 3 + 5 * 7 + 4 / 3 * (3 + 5 / 2) + 7 ** 2 - 13
print(long_mathematical_expression)


def city_greater(name, city):
    greeting = f'Witaj {name.upper()}! Miło Cię widzieć w mieście: {city.upper()}!'


if __name__ == '__main__':
    first_name = 'Jakub'
    last_name = 'Szuliński'
    email = first_name.lower() + '.' + last_name.lower() + '@gmail.com'
    print(email)

    email_formated = f'{first_name.lower()}.{last_name.lower()}@gmail.com'
    print(email_formated)

    city_greater('Michał', 'Toruń')
    city_greater('beata', 'gdynia')
