class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
        self.products.append(product)

    def get_total_price(self):

        total_price = 0
        for i in self.products:
            total_price += i.get_price()
        return total_price

    def get_total_quantity_of_products(self):

        quantities = 0
        for i in self.products:
            quantities += i.quantity
        return quantities

    def purchase(self):
        purchase = 0
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity


if __name__ == '__main__':
    customer_email = 'adrian@example.com'
    test_order = Order('adrian@example.com')


    shoes = Product('Shoes', 30.00, 3.00)
    tshirt = Product('T-shirt', 50.00, 2.00)
    bag = Product('Bag', 10.00, 1.00)



    print(shoes.name, shoes.unit_price, shoes.quantity)
    # podajemy cenę:
    shoes.get_price()
    test_order.get_total_price()

    print(test_order.products)

    # dodajemy produkty do koszyka:
    test_order.add_product(shoes)
    test_order.add_product(bag)
    test_order.add_product(tshirt)

    # pobieramy ilość produktów:
    test_order.get_total_quantity_of_products()
    print(test_order.products)

    # print(test_order.add_product(tshirt))
    print(shoes.get_price())
    print(bag.get_price())

    print(test_order.get_total_price())

    # print(test_order.customer_email)
    print(shoes.unit_price)

    print(bag.quantity)

    # print(test_order.add_product(shoes))
