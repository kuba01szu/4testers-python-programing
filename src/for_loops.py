
def print_each_student_name_capitalized(list_of_student_first_names):
    for first_name in list_of_student_first_names:
        print(first_name.capitalize())


def print_first_ten_intigers_squared():
#    first_ten_intigers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # for intiger in first_ten_intigers:
    for intiger in range(1, 11):
        print(intiger ** 2)

if __name__ == '__main__':
    list_of_students = ["kate", "marek", "toSIA", "nick", "STEPHANE"]
    print_each_student_name_capitalized(list_of_students)
    print_first_ten_intigers_squared()