
def upper_word(word):
    return word.upper()

big_dog = upper_word('dog')


if __name__ == '__main__':
    print(big_dog)
