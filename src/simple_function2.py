def is_word_contain_letter_a(word_list):
    return [word for word in word_list if 'a' in word.lower()]