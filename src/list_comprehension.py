def get_words_reversed(list_of_words):
    output_list = []

    for word in list_of_words:
        output_list.append(word[::-1])

    return output_list


def get_words_reversed_easier(list_of_words):
    return [word[::-1] for word in list_of_words]

if __name__ == '__main__':
    print(get_words_reversed(["horse", "dog", "dreams"]))
    print(get_words_reversed_easier(["horse", "dog", "dreams"]))