#Napisz funkcję, która przyjmie rocznik i przebieg samochodu i zwróci czy samochód nadal ma gwarancję.
#Jeśli wiek samochodu jest większy niż 5 lat lub przebieg jest większy niż 60 000 kilometrów - > funkcja zwraca False. W przeciwnym wypadku zwraca True.
#Przetestuj funkcję dla samochodów z danymi:
#rocznik: 2020, przebieg 30 000
#rocznik: 2020, przebieg 70 000
#rocznik: 2016, przebieg 30 000
#rocznik: 2016, przebieg 120 000


def check_guarantee(year_of_manufacture, distance):
    age = 2024 - year_of_manufacture
    year_of_manufacture = 2020
    year_of_manufacture = 2016

    if age >= 5 or distance >= 60000:
        return False
    else:
        return True


if __name__ == '__main__':
    check_guarantee(2020, 30000)
    car1 = check_guarantee(2020, 30000)
    car2 = check_guarantee(2020, 70000)
    car3 = check_guarantee(2016, 30000)
    car4 = check_guarantee(2016, 120000)
    print(car1)
    print(car2)
    print(car3)
    print(car4)
