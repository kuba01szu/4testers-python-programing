def temperature_in_celsius_to_fahrenheit(temp_celsius):
    return 9 / 5 * temp_celsius + 32

def print_each_temperature_in_Fahrenheit(temperatures_in_celsius_list):
    for temp in temperatures_in_celsius_list:
        converted_temp = round(temperature_in_celsius_to_fahrenheit(temp), 1)
        print(f"Temperatures in farenheit: {converted_temp} ")

if __name__ == '__main__':
    spring_temperatures = [10.3, 32.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print_each_temperature_in_Fahrenheit(spring_temperatures)