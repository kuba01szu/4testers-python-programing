from src.simple_function2 import is_word_contain_letter_a

def test_list_of_words_containing_letter_a():
    imput_list = ['Any', 'abrams', 'auto', 'Argentina', 'Parrot']
    expected_list = ['Any', 'abrams', 'auto', 'Argentina', 'Parrot']
    assert is_word_contain_letter_a(imput_list) == expected_list

def test_list_of_words_containing_diferent_letters():
    imput_list = ['Any', 'abrams', 'by', 'Miro', 'why', 'auto', 'Argentina', 'Parrot']
    expected_list = ['Any', 'abrams', 'auto', 'Argentina', 'Parrot']
    assert is_word_contain_letter_a(imput_list) == expected_list

def test_empty_list():
    assert is_word_contain_letter_a([]) == []

def test_list_of_words_not_containing_letter_a():
    imput_list = ['by', 'Miro', 'why']
    expected_list = []
    assert is_word_contain_letter_a(imput_list) == expected_list