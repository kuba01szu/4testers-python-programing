animal = {
    "kind": "dog",
    "age": 5,
    "male": True
}
animal2 = {
    "kind": "cat",
    "age": 2,
    "male": False
}

animal_kind = ["dog", "cat", "fish"]

animals = [
  {
    "kind": "dog",
    "age": 5,
    "male": True
  },
  {
    "kind": "cat",
    "age": 2,
    "male": False
  },
  {
    "kind": "fish",
    "age": 4,
    "male": True
  }

]

print(len(animals))
print(animals[-1])
last_animal = animals[-1]
last_animal_age = last_animal["age"]
print("The age of last animal is equal to", last_animal_age)
print(animals[0]["male"])
print(animals[1]["kind"])
print(animals[-1]["age"])
print(animals[0]["male"])

animals.append(
    {
        "kind": "zebra",
        "age": 7,
        "male": True
    }
)

print(animals)
print(len(animals))