import datetime

class Car:

    def __init__(self, mark, color, production_year):
        self.mark = mark
        self.color = color
        self.production_year = production_year
        self.milage = 0
    def repaint(self, color):
        self.color = color

    def drive(self, distance):
        self.milage += distance

    def get_age(self):
        return datetime.datetime.now().year - self.production_year

if __name__ == '__main__':
    car1 = Car('Dodge','red', 2012)
    car2 = Car('Polonez','green', 1997)
    car3 = Car('Skoda','blue', 2019)




